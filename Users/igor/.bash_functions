# grep history logs
history_grep() {
    grep "$1" ~/.log/*
}

# Coding helpers

git_status_clean()
{
    if [[ -z $(git status -s) ]]; then
        return 0
    else
        return 1
    fi
}

git_status_check()
{
    if ! git_status_clean; then
        echo 'You have uncommited changes. Aborting.'
        return 1
    fi

    return 0
}

print_and_run()
{
    echo -e "\n\e[0;36m$@:\e[0m"
    eval "$@"
}

git_local_prune()
{
    for branch in $(git for-each-ref --format '%(refname) %(upstream:track)' refs/heads | awk '$2 == "[gone]" {sub("refs/heads/", "", $1); print $1}'); do
        git branch -D $branch;
    done
}

git_fetch_pull_rebase()
{
    if ! git_status_check; then
        return 1
    fi

    current_branch=$(git rev-parse --abbrev-ref HEAD)

    branches=$(git branch)

    print_and_run "git fetch origin --prune && git_local_prune"

    if [[ "$current_branch" != "dev" ]]; then
        print_and_run "git checkout dev 2>/dev/null && git pull --rebase"
    fi

    if [[ "$current_branch" != "master" ]]; then
        print_and_run "git checkout master 2>/dev/null && git pull --rebase"
    fi

    if [[ "$current_branch" != "release" ]]; then
        print_and_run "git checkout release 2>/dev/null && git pull --rebase"
    fi

    print_and_run "git checkout $current_branch && git pull --rebase"
}

git_flow_feature()
{
    current_branch=$(git rev-parse --abbrev-ref HEAD)
    current_branch_type=$(echo $current_branch | grep '/' | cut -d '/' -f 1)
    current_branch_type=${current_branch_type:-'feature'}

    if [ "fix" == "$current_branch_type" ]; then
        current_branch_type="bugfix"
    fi

    print_and_run "git flow $current_branch_type $@"
}

git_flow_release_tag()
{
    if [ "$#" -eq 0 ]; then
        echo "Must specify a tag"
        return 1
    fi

    if ! git_status_check; then
        return 1
    fi

    current_branch=$(git rev-parse --abbrev-ref HEAD)
    tag=$1

    if [[ "$current_branch" != "dev" ]]; then
        echo "Fatal: not on dev"
        return 1
    fi

    print_and_run "git flow release start $tag"
    print_and_run "git flow release finish --nobackmerge --push $tag"
}

git_show_stash()
{
    git show stash@{$1}
}

git_exclude_skip_worktree()
{
    grep -v '^#' .git/info/exclude | while read -r file ; do
        print_and_run "git update-index --skip-worktree $file"
    done
}

git_exclude_no_skip_worktree()
{
    grep -v '^#' .git/info/exclude | while read -r file ; do
        print_and_run "git update-index --no-skip-worktree $file"
    done
}

# Cheat
function cheat()
{
    curl cht.sh/$1
}

quotes_file="/Users/igor/Documents/quotes.jsonl"

# Add a quote to the collection
add_quote() {
    if [ $# -eq 0 ]; then
        echo "  Usage: add_quote <quote> <author>"
        return
    fi

    arg_quote=$1
    arg_author=${2:-""}
    file=$quotes_file

    echo '{}' | jq -c --arg quote "$arg_quote" --arg author "$arg_author" '. + {quote: $quote, author: $author}' | tee -a "$file"
}

# Output a random quote from the collection and conclude with a welcome message
random_quote() {
    file=$quotes_file
    line="$( shuf -n 1 $file)"
    # limit width, and indent
    quote=$( echo $line | jq '.quote' | fmt --width=76 | sed 's/^/  /' )
    # remove quotation marks
    author=$( echo $line | jq '.author' | sed -e 's/^"//' -e 's/"$//' )
    # uppercase first character
    user=$( echo $USER | sed 's/./\u&/' )
    machine=$( hostname )

    # formatted and coloured output
    echo -e "
This quote comes from \e[35m${author:-an unknown author}\e[0m:

\e[36m$quote\e[0m

We are all human. Welcome to $machine, $user.
"
}
