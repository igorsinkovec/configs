# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000
HISTTIMEFORMAT="%d.%m.%y %T "

# Append history lines to the history file instead of overwriting it
setopt APPEND_HISTORY
# Correct minor spelling errors in the arguments of the cd command
setopt CORRECT
# Save each multi-line command in the history as a single entry
setopt INC_APPEND_HISTORY
# Expand aliases before executing the command
setopt ALIASES
# Include hidden files (those starting with a dot) in pathname expansions
setopt GLOB_DOTS
# Enable extended pattern matching features
setopt EXTENDED_GLOB
# Make pathname expansion case-insensitive
setopt NO_CASE_GLOB
# Perform hostname completion when the word to be completed contains a @
setopt COMPLETE_IN_WORD

# case-insensitive (all), partial-word and substring completion
autoload -Uz compinit && compinit
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'

# history search
autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end

alias code="/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin/code"

EDITOR=nano
VISUAL=code

# Load aliases
[ -f ~/.bash_aliases ]      && source ~/.bash_aliases
[ -f ~/.bash_aliases_work ] && source ~/.bash_aliases_work
[ -f ~/.bash_functions ]    && source ~/.bash_functions

# Load prompt
[ -f ~/.zsh_prompt ] && source ~/.zsh_prompt

# if typeset -t random_quote > /dev/null; then
#     random_quote
# fi

eval "$(/opt/homebrew/bin/brew shellenv)"
PATH="/opt/homebrew/opt/coreutils/libexec/gnubin:$PATH"

# log bash history
autoload -Uz add-zsh-hook
zmodload -i zsh/datetime
CMDLOGFILE=~/.log/cmd-history-$(strftime '%Y-%m-%d').log
function log_command {
    if [ "$(id -u)" -ne 0 ]; then 
        print -r "$(strftime '%Y-%m-%d %H:%M:%S %Z' $EPOCHSECONDS) $LOGNAME: $3" >> $CMDLOGFILE
    fi
}
add-zsh-hook preexec log_command

export NVM_DIR="$HOME/.nvm"
# This loads nvm
[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"
# This loads nvm bash_completion
[ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"

# Created by `pipx` on 2024-10-27 13:32:21
export PATH="$PATH:/Users/igor/.local/bin"
