#!/bin/bash
#
# Bash aliases
#
# ###

# common aliases
alias l='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias ll='ls -lh --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la='ls -lhA --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias grep='grep --color=tty -d skip'
alias cp='cp -i'	# confirm before overwriting something
alias df='df -h'	# human-readable sizes
alias du='du -h'	# human-readable sizes
alias dl='du -h -d 1'	# first level only
alias free='free -h'	# show human readable sizes
alias c="clear"
alias s='cd ..'
alias ping='ping -c 5'
# alias network-restart='sudo ifdown --exclude=lo -a && sudo ifup --exclude=lo -a'
alias more=less

# generate a password
alias pw='< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-20};echo;'

# coding helpers
alias ack="ack --color-match=bright_red --color-line=green --color-file='bold cyan'"
alias t="ack -i 'TODO|HACK|REVIEW|FIXME|DEBUG'"
alias tt='t -A 2'
alias ti="ack -i '(TODO|HACK|REVIEW|FIXME|DEBUG){1}.*?@igor'"
alias tti='ti -A 2'
alias numsort='sort | uniq -c | sort -nr'
alias lf="sed 's/\r//g' -i"
alias x="echo && git lg && echo && git st"
alias d="git diff"
alias ds="git diff --staged"
alias d-="git diff -w"
alias win2unix="sed 's/\r$//' -i"
alias unix2win="sed 's/$/\r/' -i"
alias gdiff="git --no-pager diff --no-index $1 $2"

# # set up development environment
# alias vup='vbox start && sleep 5 && vbox mount && vbox ssh'
# alias vdn='vbox unmount && vbox suspend'
# alias vs='vbox ssh'
# alias sshfs-kill='killall -9 sshfs'

# # Vagrant
alias vup='vagrant up'
# alias vrl='vagrant reload'
alias vdn='vagrant suspend'
alias vs='vagrant ssh'
# list running virtual machines
alias vms='vboxmanage list runningvms'

# Docker
alias dco='docker-compose'
alias dup='docker-compose up --detach'
alias ddn='docker-compose stop'
alias dcp="docker-compose exec php /bin/bash"
alias dcc="docker-compose run --rm composer composer"

# Git
alias god="git checkout dev"
alias gom="git checkout master"
alias gor="git checkout release"
alias gau="git add -u"
alias gaa="git add -A"
alias gap="git add -p"
alias gan="git add -N"
alias grw="git diff -w --no-color | git apply --cached --ignore-whitespace && git checkout -- . && git reset" # git reset whitespace
alias gmm="git commit --amend --no-edit" # git commend
alias gfa="git fetch origin --prune && git_local_prune"
alias ggu="git_fetch_pull_rebase"
alias ggur="git_fetch_pull_rebase && git_flow_feature rebase"
alias gs="git stash"
alias gsy="git stash --include-untracked"
alias gsm="git stash --keep-index"
alias gsp="git stash pop"
alias gss="git_show_stash"
alias gcp="git cherry-pick"
alias grrn="git rebase --continue"
alias grrs="git rebase --skip"
alias grra="git rebase --abort"
alias grrt="git checkout --theirs"
alias grro="git checkout --ours"
alias grrtheirs="git checkout --theirs . && git add -u && git rebase --continue"
alias grrours="git checkout --ours . && git add -u && git rebase --continue"
alias gff="git_flow_feature"
alias gl="git show HEAD" # git last
alias gus="git reset HEAD" # git unstage
alias gitnano="git config --global core.editor nano && echo 'Using nano as git editor.'"
alias gitcode="git config --global core.editor 'code --wait' && echo 'Using code as git editor.'"
alias galias="git aliases; alias | grep git | grep ^alias"

# Symfony
alias sfcl="sf cache:clear"

# Laravel
#alias artisan="docker-compose exec php php app/artisan"
#alias tinker="docker-compose exec php php app/artisan tinker"

# Laravel Sail
alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'
alias sup="sail up -d"
alias sdn="sail stop"
alias san="sail artisan"
